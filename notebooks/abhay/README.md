# Abhay's Worklog

[[_TOC_]]

# 2023-01-31 - First Group meeting

Having had the first group conversation today, we began to parse through several potential project ideas by considering any meaningful product to make that would fulfill a purpose that is yet to be completely solved. We went through several ideas, but found several to be already attempted by previous students of the course, or found them to be too unfamiliar to our joint skillset. Eventually, we managed to think of an idea that would help bikes and cars with detecting each other because of how rash or unfortunate driving could instantly cause danger to cyclists, particularly in zones with poor visibility, poor layout, or a multitude of other reasons. Thus, Project Sense was born!

During this session, our goal was to form a reasonable idea of what problem we’d solve. 
We managed to accomplish this goal.


# 2023-02-01 - Proposal Submission and Approval


On this day, we hedged out some final details, and eventually managed to get our proposal for the project approved! New decisions made were that the cars and bikes would each be assigned unique tag ids, and that time of flight computations would be used to accurately compute the exact distance between the car and bike. The cyclist would be told through a colored LED system, while the car driver would be told through a display (that plotted all cyclists’ positions in proximity) and a speaker that would buzz when a new cyclist is detected in range). 

During this session, our goal was to form a concrete idea of how we’d solve this problem. 
We managed to accomplish this goal.

# 2023-03-01 - Design Review

Today, we had our Design Review presented to the Professor and TAs. Having explained all our ideas, and the parts that we’d need for this, we were able to get feedback from more experienced people. As such, we learnt that certain ideas were infeasible. The idea of both the bike and the car being able to perceive their counterparts in all directions would be difficult to implement. Additionally, we began to realize the time of flight method was flawed given constraints of the equipment we could use, and thus we began to consider using GPS methods as the staff members suggested. 

Here’s a visual aid to better depict our progress at this point: 


![](Visual_Aid.png)

During this session, our goal was to deliver a successful presentation and gain valuable feedback for the future of our project.

We managed to accomplish these goals.

# 2023-04-03 - Revision Project Sense


At this point, we attempted to build with our previous ideas as a premise. However, having accidentally burned our components the previous day, and having to order new parts anyway, we began to shift towards using GPS systems for distance calculation rather than the time-of-flight method. Additionally, now working for both devices to only detect their counterparts in a single direction, the display and speakers were omitted from our plans due to redundancy. To replace their roles in the car’s PCB, a similar LED system to the bike’s PCB was now used, as the direction is evident from the scope of the project.

On this day, we aimed to finalize our design and begin research on writing code for the transceiver system.
We managed to do both.

We used links like these to understand methods of writing code for the GPS system and for the transceiver’s communication.

https://lastminuteengineers.com/neo6m-gps-arduino-tutorial/
https://learn.adafruit.com/adafruit-rfm69hcw-and-rfm96-rfm95-rfm98-lora-packet-padio-breakouts/using-the-rfm69-radio

Below is an image of the new board layout we plan on using: 

![](circuitdiagram.png)



# 2023-04-21 - Implementation Progress


Having written code and run into issues involving the GPS system’s inconsistency (it hardly ever worked in the building), we began to resolve this by further realizing that it needed unbridled access to satellites after which we allowed it to calibrate outside. This was quite cumbersome as we’d have to do multiple rounds of testing, which meant we’d have to exit the building several times per day. Nevertheless, we managed to get the transceivers to successfully communicate with one another, and when the GPS information worked, we realized that it would definitely work as the transceivers supported up to 64 bit communication. 

Today’s goal was to complete implementation of the GPS system.
We did not accomplish today’s goal due to the inconvenient nature of the testing, and so shifted focus to the transceiver due to unfavorable weather conditions.

Here’s an image of successful communication!

![](Communication.jpeg)

# 2023-04-24 - Final day of work!

At this point, considering we couldn’t particularly help the GPS’s inconsistency, we began to chalk it down to the part in question being relatively defective, as we checked if certain working conditions for the GPS were met according to its datasheet, including voltage and exposure to satellites. The conclusion of it being potentially defective was made after checking that these conditions were met. It was a pity, but we had to continue with the other parts at least. Nevertheless, we finally completed implementation of the microcontroller which would compute the distance between the two points based on GPS positions of the car and bike in question (communicated through the transceiver systems). We found that normal GPS systems do this with the Haversine formula, and programmed the microcontroller to perform this computation. Having checked before with fake input data, we knew that the LED system would work according to the input.

Today’s goal was to complete as much of the project as possible, starting with the microcontroller and its incorporation with the transceiver and GPS units. If we had time, we hoped to further refine the GPS system.
I believe we managed to accomplish this reasonably well, and performed trials for how often the GPS unit would provide data. Through comparisons with online software, we learned that the output was extremely accurate when it actually worked. 

Here’s an image for the proper derivation of the Haversine Equation used, and the final project (breadboard work and successful output), which is displaying the red LED because both systems are within 10 meters from each other. 

![](Haversine.png)

![](fulldiagram.png)

![](two.PNG)
