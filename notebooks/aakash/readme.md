# Aakash Worklog

[[_TOC_]]

# 2023-03-25 - Initial Planning and Design

We have begun working on our initial designs and plan on using time of flight in order to determine relative distance between our biker and driver. This idea stems from very similar designs I have worked on with microphones and using time of flight to determine the distance between a speaker and a microhone. For our design to function we will be ordering two pairs of transmitters and receivers in order to send time information back and forth between the biker and car devices. The RF components will operate on 415 MHz. 

RF Components: [link](https://media.digikey.com/pdf/Data%20Sheets/Seeed%20Technology/RF_Transmitter_Receiver_LinkKit_315MHz_433MHz_Web.pdf)

# 2023-04-04 - Initial Testing after receiving parts

Our components have just arrived, and I have started doing basic tests on the components to check their functionality. One basic test I completed was powering one pair of transmitters and receivers then sending some set of values to high on the transmitter, then testing the reciever's output to see if I received the right output and the that the VT or confirmation signal was high. 

![](failed_RF.png)

One major issue I ran into was that it seemed the datasheets were incorrect about their pin names. Some of the problems were that the ordering of the pin names were incorrect on the datasheet. Another odd issue was that the data pins order was flipped so if I set the transmitter D0 pin high, pin D3 would be high on the receiver. Despite these issues I was able to work around this problem and demonstrate the functionality of the RF components.

# 2023-04-05 - Secondary Testing after receiving parts

Today marked a large roadblock for our design. Yesterday, I had done some initial testing of our RF components and wanted to work with jerome to reassemble the tests and explore further testing. Yesterday I had only tested one transmitter and receiver pair. Today I wanted to test both, but while straightening the antenna as instructed it actually broke. We figured we could replace the antenna if needed since it was just a piece of wire. For that reason, we continued to reassemble our original test for the RF modules, but somehow this time around we saw a bit of smoke coming from the components. After immediately disconnecting our components, when we again only connected the power rails, there was a current of 300 mA well over the rating of 40 mA. For this reason I knew due to some improper wiring or poor part quality the components were no longer working. Below is an image of the broken components.

![](broken.jpg)

# 2023-04-06 - Early PCB Designs

At this point I was wrapping up our design on the PCB side and was thinking we just ordered more of the same parts. Although I am still a little doubtful and want to get some feedback from someone who is more aware of some of these RF components. There is not too much to say here, but here is a nice image of our gerbers. In the center you can see the ATmega. Below and to the right is the ISP connector. On the top left and right are the pin layouts for our RF modules. Lastly the remaining pins are for our resistors and LEDs.

![](Gerb_img.jpg)

# 2023-04-10 - Revising our Plan and Discussion with Jason

As a team we took the time today to do a large reassessment of our entire project. There were a number of issues that now had sprung up from our original design. Starting with the idea of using time of flight. It turns out without high precision RF equipment it is near impossible to find time of flight between two devices at such short distances. This simple has to do with the fact that given a distance of 30 meters for example, using the speed of light for the speed of propogation, the propogation time is about a tenth of a microsecond. These are scales we simply can not work on.

On top of this one problem we never considered was the fact that if two pairs of transmitters and receivers are on two devices, there would be a significant amount of cross talk if they were operating on the same frequency. 

For the above reasons we needed a new solution.

After speaking with Jason Paximadas, describing our problems, he reccomended using adafruit transcievers as they switched between transmission and reception modes eliminating cross talk and were known to be high quality. Jerome also came up with the idea to use GPS modules to determine relative positions of the bikers and drivers.

Module Datasheet: [link](https://cdn-shop.adafruit.com/product-files/3076/RFM69HCW-V1.1.pdf)

Guide for LORA: [link](https://www.mobilefish.com/developer/lorawan/lorawan_quickguide_build_lora_node_rfm95_arduino_uno.html)

Guide for GPS: [link](https://lastminuteengineers.com/neo6m-gps-arduino-tutorial/)


# 2023-04-24 - Final Design

Today we are wrapping up our final design. We were able to get the majority of our design together. Below is an image of our schematic. In the schematic you can see the connections between the ATmega and the GPS via serial communication and you can see the connection between the Mega and the transciever via SPI.

![](circuitdiagram.png)

Below is what our final breadboarded design looked like. We were able to get everything functioning except for our GPS unit. You can see in the image that the GPS modules light is on meaning it is receiving adequate voltage and current, but we were not able to have it consistently function with correct values when outdoors.

![](fullcircuit.jpg)