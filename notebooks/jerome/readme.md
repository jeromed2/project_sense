# Jerome Worklog

[[_TOC_]]

# 2023-03-25 - Discussion with Professor Choudary

Professor Choudary gave us insight on how feasible our original project idea would be. He said that in order to perform time of flight and angle of arrival calculations would be very hard to get exact since the microprocessor operates at a speed slower than the speed of light, meaning if the signal arriving is even a few feet off the calculations would be extremely off. Instead, he suggested the use of an Ultra Wideband Radio which is used in high tech location services with accuracy of being only a few feet off. The only problem is that UWB is very expensive and therefore not feasible for this project.

# 2023-04-02 - Parts Updates

We had received our receivers and transmitters so we decided to test them out. Our first time testing we were able to successfully use one pair of receiver and transmitter in order to test functionality. Yet, when we tested a second time, using the almost same exact circuit design despite an LED, we saw the current spike and our parts starting to overheat and even produce the slight bit of smoke. We knew all too soon that somehow, perhaps due to incorrect wiring or powering, that we had burnt out our transmitter and receiver. After conferring with Jason, a TA, we were able to change our project to where GPS data would be sent via a transceiver to the other system's transceiver, where the system will use the two locations to calculate which LED to turn on based off the proximity of the two.

![](broken.jpg)

[link](https://a.co/d/6uo67tD)

[link](https://a.co/d/9BuTwPs)
These are the new parts we are using and the revised circuit diagram.

![](circuitdiagram.png)



# 2023-04-21 - Testing

At long last, our parts for the new design have finally come through, and we are able to implement our design as we wanted. The first step we took was to check the functionality of our parts, and make sure they work as advertised. After following this guide [link](https://lastminuteengineers.com/neo6m-gps-arduino-tutorial/) we were able to write code and program our GPS to get readings from it! The problem is that for GPS to work, it needs access to many satellites, which is nearly impossible to do from the inside of ECE. Each time we want to test our GPS, we would have to come outside, wait ten minutes for the module to find stellites, and then a few more minutes for it to output proper location coordinates. After doing this initial GPS testing, we moved onto the transceiver, which seemed more complex at first. After following this guide [link](https://learn.adafruit.com/adafruit-rfm69hcw-and-rfm96-rfm95-rfm98-lora-packet-padio-breakouts/using-the-rfm69-radio)that adafruit laid out, we were able to successfully make the pair of transceivers talk to each other! Now comes the hard part of the project: sending gps data through a radio packet and have the microprocessor analyze it to find relative distance between the two systems.
![](rxtx.jpeg)

# 2023-04-22 - The Code

For the two transceivers to work and communicate with each other, they use an arduino library called the [link](http://www.airspayce.com/mikem/arduino/RadioHead/) Radiohead library, where examples already exist of how to do basic communication between two RFM69 modules. We did a deep dive into this type of code to determine what exactly is being sent in a transmitting and receiving radio packect. We found that the radio packet can encode 64 bits of data, meaning it could easily transmit GPS latitude and longitude. From here, all we need is the microprocessor to take the two sets of coordinates and calculate their relative distance. For this, I found that the Haversine forumla could easily take the two latitudes and longitudes and find the distance between them, since the haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes. Important in navigation, it is a special case of a more general formula in spherical trigonometry, the law of haversines, that relates the sides and angles of spherical triangles. [link](https://en.wikipedia.org/wiki/Haversine_formula)
![](haversine.png)
We decided to test the whole method out by inputting fake gps data into the tranceiver's radio packet, where the data would be sent to. tthe other system already generating a fake location. Since we already calculated that the distance between these fake sets of latitude and longitude was about a meter apart, we were looking to see if the system was able to determine this. And it turns out the transceivers worked and we were able to find the distance between the two systems using fake data. The only thing left is to make sure the real gps data is being sent instead of the fake in this situation.  


# 2023-04-24 - Wrapping it up

The last step of our project was to get the gps data from one system sent to another system and be able to calcuate relative distance to each other relatively updating every 10 ms. We ran into a problem with the gps module: it didn't want to behave how it should have. Our initial testing with it showed that it should produce location values after 10 or 15 minutes of searching for satellite signals. But after continuous testing, we found that the module would often not perform this description at all, giving blank or invalid data while the other would produce clean values. We were not able to identify what was the exact factor that could be causing it, as it may have could been a problem with the unit, problem with current or voltage drive, or a bad antenna. We decided to use fake gps data to show partial functionality, which is depicted in the photos.
Here are the two systems relaying data inside:
![](two.PNG)
And here is a diagram of the full circuit
![](fulldiagram.png)
