#include <TinyGPSPlus.h>
#include <SoftwareSerial.h>
/*
   our atmega side version of code
*/
#include <SPI.h>
#include <RH_RF95.h>

#define RFM95_CS 4
#define RFM95_RST 2
#define RFM95_INT 3

#define TEST_GPSON 8 // Pin 14 PB0
#define TEST_GPSVALID 9 // Pin 15 PB1

#define RED_LED A0
#define YELLOW_LED A1
#define GREEN_LED A2

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 915.0

static const int RXPin = 6, TXPin = 7; //decide what pins later
static const uint32_t GPSBaud = 9600;

// The TinyGPSPlus object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

void setup()
{
  Serial.begin(115200);
  ss.begin(GPSBaud);

  Serial.println(F("DeviceExample.ino"));
  Serial.println(F("A simple demonstration of TinyGPSPlus with an attached GPS module"));
  Serial.print(F("Testing TinyGPSPlus library v. ")); Serial.println(TinyGPSPlus::libraryVersion());
  Serial.println(F("by Mikal Hart"));
  Serial.println();

  pinMode(RED_LED,OUTPUT);
  pinMode(YELLOW_LED,OUTPUT);
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  delay(100);

  Serial.println("Arduino LoRa TX Test!");

  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("LoRa radio init failed");
    while (1);
  }
  Serial.println("LoRa radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
  
  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);
}

void loop()
{
  double latt;
  double longi;
  uint8_t* latt_arr;
  uint8_t* longi_arr;
  uint8_t metric_code[1];
  // This sketch displays information every time a new sentence is correctly encoded.
    //while (ss.available() > 0)
    if(ss.available()){ 
      if (gps.encode(ss.read())){
        //Serial.write(ss.read());
  
        if (gps.location.isValid())
        {
          latt = gps.location.lat();
          longi = gps.location.lng();
          latt_arr = (uint8_t*) &(latt);
          longi_arr = (uint8_t*) &(longi);
          Serial.print("Sending lattitude "); Serial.println(gps.location.lat()); delay(10);
          //metric_code[0] = 0;
          //rf95.send(metric_code,1);
          rf95.send(latt_arr, 8);
          Serial.print("Sending longitude "); Serial.println(gps.location.lng()); delay(10);
          //metric_code[0] = 1;
          //rf95.send(metric_code,1);
          rf95.send(longi_arr, 8);
          rf95.waitPacketSent();
          //displayInfo();
          digitalWrite(TEST_GPSON, HIGH);
          digitalWrite(TEST_GPSVALID, HIGH);
        }
        else
        {
          Serial.print(F("INVALID\n"));
          //latt = gps.location.lat();
          //longi = gps.location.lng();
          latt = 12.0;
          longi = 34.0;
          latt_arr = (uint8_t*) &(latt);
          longi_arr = (uint8_t*) &(longi);
          Serial.print("Sending lattitude "); Serial.println(latt); delay(10);
          //metric_code[0] = 0;
          //rf95.send(metric_code,1);
          //delay(10);
          rf95.send(latt_arr, 8);
          Serial.print("Sending longitude "); Serial.println(longi); delay(10);
          //metric_code[0] = 1;
          //f95.send(metric_code,1);
          delay(10);
          rf95.send(longi_arr, 8);
          rf95.waitPacketSent();
          //displayInfo();
          digitalWrite(TEST_GPSON, HIGH);
          digitalWrite(TEST_GPSVALID, LOW);
        }
      }
    }
  

  if (rf95.available())
  {
    // Should be a message for us now   
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf95.recv(buf, &len))
    {
      if(*buf == 0){
        digitalWrite(RED_LED, HIGH);
        digitalWrite(YELLOW_LED, LOW);
        digitalWrite(GREEN_LED, LOW);
      } else if(*buf == 1){
        digitalWrite(RED_LED, LOW);
        digitalWrite(YELLOW_LED, HIGH);
        digitalWrite(GREEN_LED, LOW);
      } else if(*buf == 2){
        digitalWrite(RED_LED, LOW);
        digitalWrite(YELLOW_LED, LOW);
        digitalWrite(GREEN_LED, HIGH);
      } else
      {
        digitalWrite(RED_LED, LOW);
        digitalWrite(YELLOW_LED, LOW);
        digitalWrite(GREEN_LED, LOW);
      }
    } else
    {
      digitalWrite(RED_LED, LOW);
      digitalWrite(YELLOW_LED, LOW);
      digitalWrite(GREEN_LED, LOW);
    }
  }
  else
  {
    Serial.println("Receive failed"); delay(10);
  }

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while(true);
  }
}

void displayInfo()
{
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Time: "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
}
